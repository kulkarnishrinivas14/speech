#include "speechIntent.hpp"

/*
 * Data Set
 */
 
std::unordered_set<std::string> places = { "paris","new york","munich", "dubai", "berlin", "york", "munich", "amsterdam", "hamburg" };
std::unordered_set<std::string> weather = { "weather","climate","temperature","cold", "rain", "sunny", "rainfall" };
std::unordered_set<std::string> fact = { "fact","knowledge","facts", "news" };
std::unordered_set<std::string> calendar = { "today", "tomorrow", "yesterday", "pm", "am", ":", "tomorrow?", "today?","yesterday?", "meeting", "meet", "appointment" };

/*
 * Function to check if input string has place
 */
bool isPlace( string s )
{
    return places.find( s ) != places.end();
}

/*
 * Function to check if input string has weather
 */
bool isWeather( string s )
{
    return weather.find( s ) != weather.end();
}

/*
 * Function to check if input string has Fact
 */
bool isFact( string s )
{
    return fact.find( s ) != fact.end();
}

/*
 * Function to check if input string has time/date/calendar
 */
bool isCalendar( string s )
{
    return calendar.find( s ) != calendar.end();
}

/*
 * Function to predict the intent from given string by performing string searching
 */
string predictiveAnalysis( vector<string> vecStrings )
{
   bool place = false;
   bool weather = false;
   bool fact = false;
   bool calendar = false;
   
   for( auto i : vecStrings )
   {
       if( isPlace( i ) )
       {
           place = true;
       }
       else if( isWeather( i ) )
       {
           weather = true;
       }
       else if( isCalendar( i ) )
       {
           calendar = true;
       }
       else if( isFact( i ) )
       {
           fact = true;
       }
       
   }
   
   if( place && weather )
   {
       return "Get Weather City";
   }
   else if( weather )
   {
       return "Get Weather";
   }
   else if( fact )
   {
       return "Get Fact";
   }
   else if( calendar )
   {
       return "Check Calendar";
   }
   else
   {
       return "No Intent Found";
   }
}

/*
 * Function to Tokenize the String
 */
void tokenize( string str, vector<string> & tokensVec )
{
   string word = "";
   for( auto token : str )
   {
       if( token == ' ')
       {
           tokensVec.push_back( word );
           word = "";
       }
       else
       {
           word = word + token ;
       }
   }
   
   tokensVec.push_back( word );
}

/*
 * getIntent()
 */
string getIntent( string str)
{  
    string intent;
    vector<string> tokens;
    transform( str.begin(), str.end(), str.begin(), ::tolower);

    ///Tokenize the string
    tokenize( str, tokens );
    
    intent = predictiveAnalysis( tokens );
      
return intent;
}




