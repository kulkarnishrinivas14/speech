#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <bits/stdc++.h> 
using namespace std;


bool isPlace( string s );
bool isWeather( string s );
bool isFact( string s );
bool isCalendar( string s );
string predictiveAnalysis( vector<string> vecStrings );
void tokenize( string str, vector<string> & tokensVec );
string getIntent( string str);




