#include "../intent/speechIntent.cpp"
#include <gtest/gtest.h>
 
TEST(IntentTest, MatchingSpeech) 
{ 
    ASSERT_EQ( "Get Weather", getIntent("What is the weather like today?"));
    ASSERT_EQ( "Get Weather City", getIntent("What is the weather like in Paris today"));
    ASSERT_EQ( "Get Weather City", getIntent("What is the weather like in New York today?"));
    ASSERT_EQ( "Check Calendar", getIntent("Am I free at 13:00 PM tomorrow?"));
    ASSERT_EQ( "Get Fact", getIntent("Tell me some interesting fact"));
}
 
TEST(IntentTest, UnmatchingSpeech) 
{
    ASSERT_EQ("No Intent Found", getIntent("What is your name"));
}
 
int main(int argc, char **argv) 
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


